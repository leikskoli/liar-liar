extends Control

@onready var left_button = $LeftButton
@onready var right_button = $RightButton


func _ready():
	# Hook up signals.
	left_button.pressed.connect(OnLeft)
	right_button.pressed.connect(OnRight)


func OnLeft():
	print("Left Click!")


func OnRight():
	print("Right click!!!!!")
