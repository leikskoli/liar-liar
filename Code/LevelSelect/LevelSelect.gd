extends Control

@onready var level_1 = $Level1
@onready var level_2 = $Level2
@onready var level_3 = $Level3
@onready var level_4 = $Level4


func _ready():
	level_1.pressed.connect(OnLevel1)
	level_2.pressed.connect(OnLevel2)


func OnLevel1():
	print("Open level 1")


func OnLevel2():
	print("Open level 2")



