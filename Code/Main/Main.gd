extends Node2D

@onready var find_liar_button = $PanelContainer/FindLiarButton


func _ready():
	find_liar_button.pressed.connect(OnLiarPressed)


func OnLiarPressed():
	print("Found a LIAR!")
